import javafx.scene.Node;
import javafx.scene.Parent;

public class BaseController {

    protected Parent view;

    public Parent getView() {
        return view;
    }

    public void setView (Parent view){
        this.view = view;
    }

    public void Show(double height, double width) {
        Client.getNavigation().Show(this, height, width);
    }

    public void Show() {
        Client.getNavigation().Show(this);
    }
}