import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.ArrayList;
import java.util.List;

public class Navigation {

    private final Stage stage;
    private final Scene scene;

    private List<BaseController> controllers = new ArrayList<>();

    public Navigation(Stage stage)
    {
        this.stage = stage;
        scene = new Scene(new Pane(), 500, 250);
        stage.setScene(scene);

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                System.exit(0);
            }
        });
    }

    public BaseController load(String sUrl)
    {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(sUrl));
            Parent root = fxmlLoader.load();

            BaseController controller = fxmlLoader.getController();
            controller.setView(root);

            return controller;
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void Show(BaseController controller, double height, double width)
    {
        try {
            stage.setHeight(height);
            stage.setWidth(width);
            stage.setResizable(true);

            scene.setRoot(controller.getView());
            controllers.add(controller);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void Show(BaseController controller)
    {
        try {
            stage.setResizable(false);

            scene.setRoot(controller.getView());
            controllers.add(controller);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void GoBack()
    {
        stage.setHeight(300);
        stage.setWidth(525);

        if (controllers.size() > 1)
        {
            controllers.remove(controllers.get(controllers.size() - 1));
            scene.setRoot(controllers.get(controllers.size() - 1).getView());
        }
    }
}