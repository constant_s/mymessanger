import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class ClientController extends BaseController implements Initializable{

    public static final String URL_FXML = "client.fxml";

    static public StartClient.Item resiver = null;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1){
        Client.StartClient();
        username.setText(Client.getUsername());
        sent.setOnAction(event -> SendMessage());
        message.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(keyEvent.getCode() == KeyCode.ENTER)
                    SendMessage();
            }
        });
        try {
            list.setItems(FXCollections.observableList(StartClient.users));

            StartClient.users.addListener(new ListChangeListener() {
                @Override
                public void onChanged(ListChangeListener.Change change) {
                    list.setItems(FXCollections.observableList(StartClient.users));
                    username.setText(Client.getUsername());
                }
            });
                list.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<StartClient.Item>() {
                    public void changed(ObservableValue<? extends StartClient.Item> ov, StartClient.Item old_val, StartClient.Item new_val) {
                        textfield.clear();
                        resiver = ov.getValue();
                        if (!StartClient.users.isEmpty()) {
                            ObservableList<String> h = ov.getValue().getHistory();
                            for (String s : h) {
                                textfield.appendText(s + "\n");
                            }
                            ov.getValue().getHistory().addListener(new ListChangeListener() {
                                @Override
                                public void onChanged(ListChangeListener.Change change) {
                                    textfield.clear();
                                    ObservableList<String> h = ov.getValue().getHistory();
                                    for (String s : h) {
                                        textfield.appendText(s + "\n");
                                    }
                                }
                            });
                        }
                    }
                });
        }
        catch (Exception e) {
//            e.printStackTrace();
        }

        about.setOnAction(event -> {
            textfield.appendText("Author: Sosonnyi Kostia. KN-14-6. 2016\n");
        });

        reconnect.setOnAction(event -> {
            StartClient.SendExit();
            Client.getNavigation().GoBack();
        });

        exit.setOnAction(event -> {
            StartClient.SendExit();
            System.exit(0);
        });
    }

    @FXML
    private ListView<StartClient.Item> list;

    @FXML
    private TextArea textfield;

    @FXML
    private MenuItem reconnect;

    @FXML
    private MenuItem about;

    @FXML
    private MenuItem exit;

    @FXML
    private Button sent;

    @FXML
    private TextField username;

    @FXML
    private TextField message;

    @Override
    public void Show() {
        super.Show(600,  900);
    }

    private void SendMessage(){
        if(resiver != null && !message.getText().trim().isEmpty()) {
            StartClient.SendMessage(resiver.getUser(), username.getText() + ": " + message.getText());
            AddHistory(resiver.getUser(), username.getText() + ": " + message.getText());
            message.clear();
        }
    }

    public static void AddHistory(String user, String message){
        for (StartClient.Item item : StartClient.users) {
            if (item.getUser().equals(user)) {
                item.setHistory(message);
                break;
            }
        }
    }
}
