import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClientStartController extends BaseController implements Initializable{

    public static final String URL_FXML = "clientStart1.fxml";

    private final String IP_ADDRESS_PATTERN
            = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    private final Pattern pattern = Pattern.compile(IP_ADDRESS_PATTERN);;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1){
        try {
            label.setVisible(false);
            accept.setOnAction(event -> LogIn());
            username.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent keyEvent) {
                    if (keyEvent.getCode() == KeyCode.ENTER)
                        LogIn();
                }
            });
            ip.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent keyEvent) {
                    if (keyEvent.getCode() == KeyCode.ENTER)
                        LogIn();
                }
            });
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public boolean ValidateUsername(String userNameString){
        Pattern p = Pattern.compile("^[A-Za-z0-9_-]{3,15}$");
        Matcher m = p.matcher(userNameString);
        return m.matches();
    }

    public boolean ValidateIP(String ipAddress) {
        return pattern.matcher(ipAddress).matches();
    }

    private void LogIn() {
        try {
            if (ValidateIP(ip.getText()) && ValidateUsername(username.getText())) {
                if(Client.SetUsername(username.getText(), ip.getText()))
                    if(username.getText().equals("AI1"))
                        Client.getNavigation().load(ClientControllerB.URL_FXML).Show();
                    else
                        Client.getNavigation().load(ClientController.URL_FXML).Show();
                else{
                    label.setText("Server don't response\n");
                    label.setVisible(true);
                    System.out.print("Wrong data\n");
                }
            } else {
                label.setText("Invalid data\n");
                label.setVisible(true);
                System.out.print("Wrong data\n");
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @FXML
    private TextField username;

    @FXML
    private TextField ip;

    @FXML
    private Button accept;

    @FXML
    private Label label;
}