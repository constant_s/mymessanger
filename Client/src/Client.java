import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

public class Client extends Application {

    static final int PORT = 666;
    static private PrintWriter out;
    static private Socket socket;
    static public String username = "";

    static private StartClient client;

    private static Navigation navigation;

    public static Navigation getNavigation()
    {
        return navigation;
    }

    public static void main(String[] args) throws IOException {
        launch(args);
    }

    public static Socket getSocket(){
        return socket;
    }

    public static String getUsername() {
        return username;
    }

    public static PrintWriter getOut() {
        return out;
    }

    public static boolean SetUsername(String name, String ip){
        try {
            username = name;
            socket = new Socket(ip, PORT);
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            out.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><message type=\"set_names\" sender=\"" + username + "\" receiver=\"Server\" content=\"Text\"></message>");
            return true;
        }
        catch (IOException e){
//            e.printStackTrace();
        }
        return false;
    }
    static public void StartClient(){
        client = new StartClient();
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        try {
            navigation = new Navigation(primaryStage);
            primaryStage.setTitle("MyMessenger");
            primaryStage.show();
            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                    if(!username.isEmpty())
                        StartClient.SendExit();
                    System.exit(0);
                }
            });
            Client.getNavigation().load(ClientStartController.URL_FXML).Show();
        }
        catch(Exception e){
//            e.printStackTrace();
        }
    }

}

class StartClient extends Thread {
    static private BufferedReader in;

    static public  ObservableList<StartClient.Item> users = FXCollections.observableArrayList();

    public StartClient(){
        start();
    }

    public void run(){
        try {
            in = new BufferedReader(new InputStreamReader(Client.getSocket().getInputStream()));
            while (true) {
                String message = in.readLine();
                SAXParserFactory factory = SAXParserFactory.newInstance();
                SAXParser parser = factory.newSAXParser();
                SAX saxp = new SAX();
                InputStream is = new ByteArrayInputStream(message.getBytes());
                parser.parse(is, saxp);
                switch (saxp.getType()) {
                    case "send_message":
                        ClientController.AddHistory(saxp.getReceiver(), saxp.getContext());
                        break;
                    case "set_names":
                        System.out.println("New Name: " + this.getName());
                        break;
                    case "update_user_list":
                        UpdateUserList(saxp.getContext());
                        break;
                    case "update_username":
                        Client.username = saxp.getContext();
                        break;
                    case "update_user_list_d":
                        UpdateUserListD(saxp.getContext());
                        break;
                    default:
                        break;
                }
            }
        }
        catch (Exception e) {
//            e.printStackTrace();
            Client.getNavigation().GoBack();
        }
    }

    public static void SendMessage(String receiver, String message) {
        try {
            String buf = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><message type=\"" + "send_message" + "\" sender=\"" + Client.getUsername() + "\" receiver=\"" + receiver + "\" content=\"" + message + "\"></message>";
            Client.getOut().println(buf);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void UpdateUserList(String user){
        boolean b = false;
        for (Item item: users) {
            if(item.getUser().equals(user)) {
                b = true;
                break;
            }
        }
        if(!b) users.add(new Item(user));
    }

    public void UpdateUserListD(String user){
        for (Item item: users) {
            if(item.getUser().equals(user)) {
                users.remove(item);
                break;
            }
        }
    }
    public static void SendExit() {
        try {
            String buf = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><message type=\"" + "delete_user" + "\" sender=\"" + Client.getUsername() + "\" receiver=\"" + "Server" + "\" content=\"" + "" + "\"></message>";
            Client.getOut().println(buf);
            users.clear();
        }
        catch (Exception e) {
//            e.printStackTrace();
        }
    }
    public static class Item {

        private final StringProperty user = new SimpleStringProperty(this, "user");
        private ObservableList<String> history = FXCollections.observableArrayList();

        public Item(String user) {
            this.setUser(user);
        }

        public final void setUser(String user) {
            this.user.set(user);
        }

        public void setHistory(String message) {
            history.add(message);
        }

        public final String getUser() {
            return user.get();
        }

        public ObservableList<String> getHistory() {
            return history;
        }

        @Override
        public String toString() {
            return String.format(user.get());
        }
    }
}