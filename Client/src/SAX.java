import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAX extends DefaultHandler{

    String sender, receiver, type, context;

    String getSender(){
        return sender;
    }
    String getReceiver(){
        return receiver;
    }
    String getContext(){
        return context;
    }
    String getType(){
        return type;
    }

    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        for (int i = 0; i < atts.getLength(); i++) {
            switch (atts.getLocalName(i)) {
                case "sender":
                    sender = atts.getValue(i);
                    break;
                case "receiver":
                    receiver = atts.getValue(i);
                    break;
                case "type":
                    type = atts.getValue(i);
                    break;
                case "content":
                    context = atts.getValue(i);
                    break;
                default:
                    break;
            }
        }

    }
}
