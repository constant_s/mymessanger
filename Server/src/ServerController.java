import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ServerController implements Initializable {

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        try {
            list.setItems(FXCollections.observableList(StartServer.conv));

            StartServer.conv.addListener(new ListChangeListener() {
                @Override
                public void onChanged(ListChangeListener.Change change) {
                    list.setItems(FXCollections.observableList(StartServer.conv));
                }
            });

            list.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<StartServer.Item>() {
                public void changed(ObservableValue<? extends StartServer.Item> ov, StartServer.Item old_val, StartServer.Item new_val) {
                    area.clear();
                    ObservableList<String> h = ov.getValue().getHistory();
                    for (String s : h) {
                        area.appendText(s + "\n");
                    }
                    ov.getValue().getHistory().addListener(new ListChangeListener() {

                        @Override
                        public void onChanged(ListChangeListener.Change change) {
                            area.clear();
                            ObservableList<String> h = ov.getValue().getHistory();
                            for (String s : h) {
                                area.appendText(s + "\n");
                            }
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        about.setOnAction(event -> {
            area.appendText("Author: Sosonnyi Kostia. KN-14-6. 2016\n");
        });

        exit.setOnAction(event -> System.exit(0));
    }

    public static void AddHistory(String user1, String user2, String message){
        for (StartServer.Item item : StartServer.conv) {
            if ((item.getUser1().equals(user1) && item.getUser2().equals(user2)) || (item.getUser1().equals(user2) && item.getUser2().equals(user1))) {
                item.setHistory(message);
                break;
            }
        }
    }

    @FXML
    private ListView<StartServer.Item> list;

    @FXML
    private TextArea area;

    @FXML
    private MenuItem about;

    @FXML
    private MenuItem exit;

    @FXML
    public void onClickMethod() {

    }

}
