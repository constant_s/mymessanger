import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import javafx.stage.WindowEvent;
import org.xml.sax.SAXException;

class Execution extends Thread {

    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    public Execution(Socket s, int i) throws IOException {
        socket = s;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        this.setName("User" + i);
        start();
    }

    public void Out(String message){
        out.println(message);
    }

    public void WarnUsername(){
        String message = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><message type=\"" + "update_username" + "\" sender=\"" + "Server" + "\" receiver=\"" + this.getName() + "\" content=\"" + this.getName()  + "\"></message>";
        out.println(message);
    }

    public void UpdUserList(String user){
        String buf = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><message type=\"" + "update_user_list" + "\" sender=\"" + "Server" + "\" receiver=\"" + this.getName() + "\" content=\"" + user + "\"></message>";
        StartServer.Out(this.getName(), buf);
    }

    public void UpdUserListD(String user){
        String buf = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><message type=\"" + "update_user_list_d" + "\" sender=\"" + "Server" + "\" receiver=\"" + this.getName() + "\" content=\"" + user + "\"></message>";
        StartServer.Out(this.getName(), buf);
    }

    public void run() {
        try {
            while (true) {
                String message = in.readLine();
                SAXParserFactory factory = SAXParserFactory.newInstance();
                SAXParser parser = factory.newSAXParser();
                SAX saxp = new SAX();
                InputStream is = new ByteArrayInputStream(message.getBytes());
                parser.parse(is, saxp);
                switch(saxp.getType()) {
                    case "send_message":
                        String buf = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><message type=\"send_message\" sender=\"" + saxp.getReceiver() + "\" receiver=\"" + this.getName() + "\" content=\"" + saxp.getContext() + "\"></message>";
                        StartServer.Out(saxp.getReceiver(), buf);
                        StartServer.AddConv(this.getName(), saxp.getReceiver());
                        StartServer.AddHistory(this.getName(), saxp.getReceiver(), saxp.getContext());
                        break;
                    case "set_names":
                        if(StartServer.CheckName(saxp.getSender()))
                            this.setName(saxp.getSender());
                        else WarnUsername();
                        StartServer.UpdateUsers();
                        System.out.println("New Name: " + this.getName());
                        break;
                    case "delete_user":
                        System.out.println("Delete User: " + this.getName());
                        StartServer.DeleteUser(this.getName());
                        return;
                    default: break;
                }
            }
        }
        catch (Exception e) {
            System.err.println("Exception");
            e.printStackTrace();
        }
        finally {
            try {
                socket.close();
            }
            catch (IOException e) {
                e.printStackTrace();
                System.err.println("Socket not closed");
            }
        }
    }
}

public class Server extends Application {

    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("server1.fxml"));
            primaryStage.setTitle("MyMessenger[Server]");
            primaryStage.setScene(new Scene(root));
            primaryStage.show();

            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                    System.exit(0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        new StartServer();
    }
}

class StartServer extends Thread {

    static final int PORT = 666;

    static public ObservableList<Item> conv = FXCollections.observableArrayList();

    private static ArrayList<Execution> exec = new ArrayList<>();

    public StartServer(){
        start();
    }

    static void Out(String name, String message) {
        for (Execution e : exec)
            if (e.getName().equals(name))
                e.Out(message);
    }
    public void run(){
        try {
            ServerSocket s = new ServerSocket(PORT);
            while (true) {
                Socket socket = s.accept();
                try {
                    exec.add(new Execution(socket, exec.size()));
                }
                catch (IOException e) {
                    socket.close();
                }
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    static public void AddConv(String user1, String user2) {
        try {
            if (conv.isEmpty()) {
                conv.add(new StartServer.Item(user1, user2));
            }
            else {
                boolean b = false;
                for (Item item : conv) {
                    if ((item.getUser1().equals(user1) && item.getUser2().equals(user2)) | (item.getUser1().equals(user2) && item.getUser2().equals(user1))) {
                        b = true;
                        break;
                    }
                }
                if (!b)
                    conv.add(new StartServer.Item(user1, user2));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    static public void DeleteUser(String user){
        try {
            for (Execution e : exec) {
                if (e.getName().equals(user)) {
                    exec.remove(e);
                    break;
                }
            }
            try {
                for (Item item : conv) {
                    if (item.getUser1().equals(user) | item.getUser2().equals(user))
                        conv.remove(item);
                }
            }
            catch (ConcurrentModificationException e) {
//                e.printStackTrace();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        for (Execution e : exec)
            e.UpdUserListD(user);
    }

    static public void UpdateUsers() {
        for (Execution e : exec)
            for (Execution x : exec)
                    if (!e.getName().equals(x.getName()))
                        e.UpdUserList(x.getName());
    }

    static public void AddHistory(String user1, String user2, String message){
        ServerController.AddHistory(user1, user2, message);
    }

    static public boolean CheckName(String name){
        for (Execution e : exec)
            if(e.getName().equals(name))
                return false;
        return true;
    }

    public static class Item {
        private final StringProperty user1 = new SimpleStringProperty(this, "user1");
        private final StringProperty user2 = new SimpleStringProperty(this, "user2");
        private ObservableList<String> history = FXCollections.observableArrayList();

        public Item(String user1, String user2) {
            this.setUser1(user1);
            this.setUser2(user2);
        }

        public final void setUser1(String user) {
            this.user1.set(user);
        }

        public final void setUser2(String user) {
            this.user2.set(user);
        }

        public void setHistory(String message) {
            history.add(message);
        }

        public final String getUser1() {
            return user1.get();
        }

        public final String getUser2() {
            return user2.get();
        }

        public ObservableList<String> getHistory() {
            return history;
        }

        @Override
        public String toString() {
            return String.format("%s <-> %s", user1.get(), user2.get());
        }
    }
}